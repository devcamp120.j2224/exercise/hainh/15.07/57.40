public class SuperPersonClass {
    public static void main(String[] args) {
        Person person1 = new Person("Hải", " tôn thất thuyết ");
        Person person2 = new Person("Hoàng Hải ", "Q4");

        System.out.println( "person1: " + person1.toString() + "\n" +
                            "person2: " + person2.toString());

        Student student1 = new Student("Hải", "Tôn Thất Thuyết ", "Developer", 1990, 2.0);
        Student student2 = new Student("Hoàng Hải", "Q4 ", "Sofware Engineer", 1990, 3.0);

        
        System.out.println( "student1: " + student1.toString() + "\n" +
                            "student2: " + student2.toString());

        Staff staff1 = new Staff("Hải", "Tôn Thất Thuyết", "IronHack", 3.0) ; 
        Staff staff2 = new Staff("Hoàng Hải", "Q4", "IronHack 120 Ngày", 4.0) ; 
        
        System.out.println(  "staff1: " + staff1.toString() + "\n" +
                             "staff2: " + staff2.toString());

    }
}
