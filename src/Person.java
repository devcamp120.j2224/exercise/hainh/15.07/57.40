public class Person {
    String name ;
    String adress ;

    public String getAdress() {
        return adress;
    }


    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getName() {
        return name;
    }

    public Person(String name, String adress) {
        this.name = name;
        this.adress = adress;
    }

    @Override
    public String toString() {
        return "Person [adress= " + adress + ", name= " + name + "]";
    }
    
}
